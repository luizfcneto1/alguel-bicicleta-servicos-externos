package services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import models.CartaoDeCredito;
import models.Erro;

public class ServiceCCTest extends JUnitCore {

	@Test
	public void servicoValidaCartaoSucesso() {
		Object response = CCService.validaCartao( "{ \"nomeTitular\": \"Luiz Fernando\", \"numero\": \"4024.0071.5376.3191\", \"validade\": \"2021-05-03\", \"cvv\": 613 }" );
		Assert.assertEquals( CartaoDeCredito.class , response.getClass() );
	}
	
	@Test
	public void servicoValidaCartaoFalha404() {
		Object response = CCService.validaCartao( "{ \"nomeTitular\": \"Luiz Fernando\", \"numero\": \"\", \"validade\": \"2021-05-03\", \"cvv\": 613 }" );
		Assert.assertEquals( Erro.class , response.getClass() );
	}
	
	@Test
	public void servicoValidaCartaoFalha422() {
		Object response = CCService.validaCartao( "{ \"nomeTitular\": \"Luiz Fernando\", \"numero\": \"123456789\", \"validade\": \"2021-05-03\", \"cvv\": 613 }" );
		Assert.assertEquals( Erro.class , response.getClass() );
	}

}
