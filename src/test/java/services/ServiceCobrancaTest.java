package services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import models.Cobranca;
import models.Erro;

public class ServiceCobrancaTest extends JUnitCore {

	@Test
	public void servicoEfetuarCobrancaSucesso() {
		Object response = CobrancaService.efetuarCobranca(
				"{\n" + 
				"    \"valor\": 1,\n" + 
				"    \"ciclista\": \"f08e53a3-7a4f-47bd-9b93-baeedc490dd1\"\n" + 
				"}"
				);
		Assert.assertEquals( Cobranca.class, response.getClass() );
	}
	
	@Test
	public void servicoEfetuarCobrancaFalhaIntegracao404() {
		Object response = CobrancaService.efetuarCobranca(
				"{\n" + 
				"    \"valor\": 0,\n" + 
				"    \"ciclista\": \"123\" \n" +
				"}"
				);
		Assert.assertEquals( Erro.class, response.getClass() );	
	}
	
	@Test
	public void servicoEfetuarCobrancaFalha422() {
		Object response = CobrancaService.efetuarCobranca(
				"{\n" + 
				"    \"valor\": 0,\n" + 
				"    \"ciclista\": \"\" \n" +
				"}"
				);
		Assert.assertEquals( Erro.class, response.getClass() );	
	}
	
	@Test
	public void servicoAddCobrancaListaSucesso() {
		Object response = CobrancaService.addCobrancaLista(
				"{\n" + 
						"    \"valor\": 1,\n" + 
						"    \"ciclista\": \"f08e53a3-7a4f-47bd-9b93-baeedc490dd1\"\n" + 
						"}"
				);
		Assert.assertEquals( Cobranca.class, response.getClass());
	}
	
	@Test
	public void servicoAddCobrancaListaFalha422() {
		Object response = CobrancaService.addCobrancaLista(
				"{\n" + 
						"    \"valor\": 0,\n" + 
						"    \"ciclista\": \"\"\n" + 
						"}"
				);
		Assert.assertEquals( Erro.class , response.getClass() );
	}
	
	@Test
	public void servicoGetCobrancaSucesso() {
		this.servicoAddCobrancaListaSucesso();
		Object response = CobrancaService.getCobranca( "f08e53a3-7a4f-47bd-9b93-baeedc490dd1" );
		Assert.assertEquals( Cobranca.class , response.getClass() );
	}
	
	@Test
	public void servicoGetCobrancaFalha() {
		Object response = CobrancaService.getCobranca( "f08e53a3-7a4f-47bd-9b93-baeedc490dd1" );
		Assert.assertEquals( Cobranca.class , response.getClass() );
	}
	
	@Test
	public void servicoGetCobrancaFalhaCobrancaNaoEncontrada() {
		Object response = CobrancaService.getCobranca( "1234213" );
		Assert.assertEquals( Erro.class , response.getClass() );
	}
	
	@Test
	public void servicoGetCobrancaFalhaCobrancaVazia() {
		Object response = CobrancaService.getCobranca( "" );
		Assert.assertEquals( Erro.class , response.getClass() );
	}
	
	
	
}
