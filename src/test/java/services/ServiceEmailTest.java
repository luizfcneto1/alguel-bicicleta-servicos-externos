package services;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import models.Email;
import models.Erro;


public class ServiceEmailTest extends JUnitCore{

//	servicoEnviarEmailSucesso
	@Test
	public void servicoEniarEmailSucesso() {
		Object response = EmailService.enviarEmail( "{ \"email\": \"luiz.cunha@uniriotec.br\", \"mensagem\": \"Teste de servico de email\" }" );
		Assert.assertEquals( Email.class, response.getClass()  );
	}
	
//	enviarEmail404
	@Test
	public void servicoEnviarEmailErro404() {
		Object response = EmailService.enviarEmail( "{ \"email\": \"\", \"mensagem\": \"Isso aqui n�o deve funcionar\" }" ); 
		Assert.assertEquals( Erro.class, response.getClass() );
	}

	
//	enviarEmail412
	@Test
	public void servicoEnviarEmailErro412() {
		Object response = EmailService.enviarEmail( "{ \"email\": \"@email@email.\", \"mensagem\": \"Email nao ser� enviado\" }" );
		Assert.assertEquals( Erro.class, response.getClass() );
	}
	
}
