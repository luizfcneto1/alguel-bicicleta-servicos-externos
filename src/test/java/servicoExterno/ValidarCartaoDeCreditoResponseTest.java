package servicoExterno;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;


import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class ValidarCartaoDeCreditoResponseTest extends JUnitCore{
		
//	test validaCartao200
	@Test
	public void validaCartao200() {
		HttpResponse<String> response = Unirest.get("https://grupo3-aluguel.herokuapp.com/cartaoDeCredito/" 
				+ "f08e53a3-7a4f-47bd-9b93-baeedc490dd1" ).asString();
		Assert.assertEquals( response.getStatus(), 200 );
	}
	
//	test validaCartao404()
	@Test
	public void validaCartao404() {
		HttpResponse<String> response = Unirest.get("https://grupo3-aluguel.herokuapp.com/cartaoDeCredito/123").asString();
		Assert.assertEquals( response.getStatus(), 404 );
	}


}
