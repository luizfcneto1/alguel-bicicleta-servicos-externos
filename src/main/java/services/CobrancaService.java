package services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.google.gson.Gson;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import models.CartaoDeCredito;
import models.Cobranca;
import models.Erro;

public class CobrancaService {

	private static ArrayList<Cobranca> filaCobranca = new ArrayList<Cobranca>();

	
	private static HttpResponse<CartaoDeCredito> getCartaoDeCreditoDoCiclista( String idCiclista ){
		return 	Unirest
				.get("https://grupo3-aluguel.herokuapp.com/cartaoDeCredito/{idCiclista}")
				.header("Accept", "application/json").routeParam("idCiclista", idCiclista )
				.asObject(CartaoDeCredito.class);
	}
	
	public static Object efetuarCobranca(String body) {

		Gson gson = new Gson();
		Cobranca cobranca = gson.fromJson(body, Cobranca.class);

		HttpResponse<CartaoDeCredito> ccResponse = getCartaoDeCreditoDoCiclista( cobranca.getCiclista() );

		if (ccResponse.getStatus() == 404 || ccResponse.getStatus() == 500) {
			Erro erroCartao = new Erro(String.valueOf(cobranca.getCiclista()), "404", "Dados Invalidos");
			return erroCartao;

		} else {
			int statusCobraCartao = validateCobrancaCartao(cobranca);
			if (statusCobraCartao == 200) {

				Cobranca novaCobranca = new Cobranca(cobranca.getValor(), cobranca.getCiclista());

				Date dateIni = Calendar.getInstance().getTime();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

				String horaSolicitacao = dateFormat.format(dateIni);
				novaCobranca.setHoraSolicitacao(horaSolicitacao.toString());

				Date dateFin = Calendar.getInstance().getTime();
				String horaFinalizacao = dateFormat.format(dateFin);
				novaCobranca.setHoraFinalizacao(horaFinalizacao.toString());

				return novaCobranca;

			} else {
				Erro erro = new Erro(String.valueOf(cobranca.getId()), String.valueOf(statusCobraCartao),
						"Dados Invalidos");
				return erro;
			}
		}

	}

	public static Object getCobranca(String pathParamId) {

		int statusGetCobranca = validateGetCobranca(pathParamId);

		if (statusGetCobranca == 200) {
			Cobranca cobranca = getCobrancaFromList(pathParamId);

			if (cobranca == null) {
				Erro erro = new Erro( "", String.valueOf(statusGetCobranca),
						"Nao Encontrado");
				return erro;
			} else {
				return cobranca;
			}

		} else {
			Erro erro = new Erro("", String.valueOf(statusGetCobranca), "Nao Encontrado");
			return erro;
		}
	}

	private static Cobranca getCobrancaFromList(String id) {
		Cobranca temp = null;
		for (Cobranca cobranca : filaCobranca) {
			if (cobranca.getCiclista().equals(id)) {
				temp = cobranca;
				break;
			}
		}
		return temp;
	}

	public static Object addCobrancaLista(String body) {

		Gson gson = new Gson();
		Cobranca cobranca = gson.fromJson(body, Cobranca.class);

		int statusCobranca = validateAddCobrancaFila(cobranca);
		if (statusCobranca == 200) {

			Cobranca novaCobranca = new Cobranca(cobranca.getValor(), cobranca.getCiclista());

			Date dateIni = Calendar.getInstance().getTime();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

			String horaSolicitacao = dateFormat.format(dateIni);
			novaCobranca.setHoraSolicitacao(horaSolicitacao.toString());

			Date dateFin = Calendar.getInstance().getTime();
			String horaFinalizacao = dateFormat.format(dateFin);
			novaCobranca.setHoraFinalizacao(horaFinalizacao.toString());
			filaCobranca.add(novaCobranca);

			return novaCobranca;
		} else {
			Erro erro = new Erro(String.valueOf(cobranca.getId()), String.valueOf(statusCobranca), "Dados Invalidos");
			return erro;
		}

	}
	
	public static int validateCobrancaCartao( Cobranca cobranca ) {
		if( cobranca.getValor() > 0 && cobranca.getCiclista() != null && cobranca.getCiclista().length() > 0 )
			return 200;
		else 
			return 422;
	}
	
	public static int validateGetCobranca( String idCobranca ) {
		if( idCobranca.length() != 0 )
			return 200;
		else 
			return 404;
	}
	
	public static int validateAddCobrancaFila( Cobranca cobranca ) {
		if( cobranca.getValor() > 0 && cobranca.getCiclista() != null && cobranca.getCiclista().length() > 0 )
			return 200;
		else
			return 422;
	}
}
