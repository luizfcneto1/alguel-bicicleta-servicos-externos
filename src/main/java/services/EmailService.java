package services;

import com.google.gson.Gson;

import io.javalin.http.Context;
import models.Email;
import models.Erro;
import servicoExterno.MailServer;

public class EmailService {

	private static void enviaEmail(Email email) {
		MailServer serverEmail = MailServer.getInstance();
		serverEmail.criaMensagem(email.getEmail(), "Notificacao Enviada Automaticamente", email.getMensagem());
		serverEmail.enviaEmail();
	}

	public static Object enviarEmail(String body) {

		System.out.println(body);
		Gson gson = new Gson();
		Email email = gson.fromJson(body, Email.class);

		int emailStatus = validateEmail(email);

		if (emailStatus == 200) {
			Email emailNovo = new Email(email.getEmail(), email.getMensagem());
			enviaEmail(emailNovo);
			return emailNovo;

		} else if (emailStatus == 404) {
			Erro erro = new Erro(String.valueOf(email.getId()), String.valueOf(emailStatus), "E-mail nao existe");
			return erro;
		} else {
			Erro erro = new Erro(String.valueOf(email.getId()), String.valueOf(emailStatus),
					"E-mail com formato invalido");
			return erro;
		}
	}

	public static int validateEmail(Email email) {

		if (email.getEmail() == null || email.getEmail().length() == 0)
			return 404;

		String[] arrEmail = email.getEmail().trim().split("@");
		if (arrEmail.length == 1 || arrEmail.length > 2)
			return 422;

		return 200;
	}

}
