package services;

import com.google.gson.Gson;

import models.CartaoDeCredito;
import models.Erro;

public class CCService {

	public static Object validaCartao(String body) {

		Gson gson = new Gson();
		CartaoDeCredito cc = gson.fromJson(body, CartaoDeCredito.class);

		int verificaValidacaoCC = validateCartaoCredito(cc);

		if (verificaValidacaoCC == 200) {
			CartaoDeCredito ccNovo = new CartaoDeCredito(cc.getNomeTitular(), cc.getNumero(), cc.getValidade(),
					cc.getCvv(), cc.getId());
			return ccNovo;

		} else {
			if (verificaValidacaoCC == 404) {
				Erro erro = new Erro(String.valueOf(cc.getId()), String.valueOf(verificaValidacaoCC), "No Encontrado");
				return erro;
			} else {
				Erro erro = new Erro(String.valueOf(cc.getId()), String.valueOf(verificaValidacaoCC), "Dados Invlidos");
				return erro;
			}
		}

	}

	public static int validateCartaoCredito(CartaoDeCredito cc) {

//	"4024.0071.5376.3191" exemplo de um cartao de teste, ler Zero Auth
		if (cc.getNumero().length() == 19) {
			return 200;
		} else if (cc.getNumero().length() == 0) {
			return 404;
		} else {
			return 422;
		}
	}
}
