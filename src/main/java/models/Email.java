package models;

import java.util.UUID;

public class Email {

	private String id;
	private String email;
	private String mensagem;
	
	public Email( String email, String mensagem ) {
		this.email = email;
		this.mensagem = mensagem;
		
		UUID uuid = UUID.randomUUID();
		this.id = uuid.toString();
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getMensagem() {
		return mensagem;
	}

}


