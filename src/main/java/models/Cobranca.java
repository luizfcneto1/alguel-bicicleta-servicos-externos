package models;

import java.util.UUID;

public class Cobranca {
	private String id;
	private String status;
	private String horaSolicitacao;
	private String horaFinalizacao;
	private double valor;			// 0.01, menor valor poss�vel: 0
	private String ciclista;		// uuid do ciclista
	
	public Cobranca( double valor, String ciclista ) {
		this.valor = valor;
		this.ciclista = ciclista;
		
		UUID uuid = UUID.randomUUID();
		this.id = uuid.toString();
	}

	public String getId() {
		return id;
	}

	public void setHoraSolicitacao(String horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}


	public void setHoraFinalizacao(String horaFinalizacao) {
		this.horaFinalizacao = horaFinalizacao;
	}

	public double getValor() {
		return valor;
	}


	public String getCiclista() {
		return ciclista;
	}

			
}
