package models;

public class Erro {
	
	private String id;
	private String codigo;
	private String mensagem;
	
	public Erro(String id, String codigo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;
		this.id = id;
	}
	
}
