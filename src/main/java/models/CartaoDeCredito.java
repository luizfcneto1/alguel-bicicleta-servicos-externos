package models;

public class CartaoDeCredito {
	private String id;
	private String nomeTitular;
	private String numero;
	private String validade;
	private String cvv;
	
	public CartaoDeCredito( String nomeTitular,String  numero, String validade, String cvv, String id ) {
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
		this.cvv = cvv;
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public String getNomeTitular() {
		return nomeTitular;
	}

	public String getNumero() {
		return numero;
	}

	public String getValidade() {
		return validade;
	}

	public String getCvv() {
		return cvv;
	}

}
