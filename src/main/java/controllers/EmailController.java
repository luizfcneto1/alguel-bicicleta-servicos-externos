package controllers;

import io.javalin.http.Context;
import services.EmailService;

public class EmailController {

	public static void enviarEmail(Context ctx) {

		try {
			String body = ctx.body();
			Object emailResponse = EmailService.enviarEmail(body);
			ctx.json(emailResponse);
		} catch (Exception error) {
			error.getMessage();
			ctx.json("{\n" + "  \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\",\n" + "  \"codigo\": \"string\",\n"
					+ "  \"mensagem\": " + error.getMessage() + "  \n" + "}");
		}
	}
}
