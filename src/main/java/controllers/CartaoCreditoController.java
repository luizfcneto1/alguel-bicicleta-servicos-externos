package controllers;


import io.javalin.http.Context;
import services.CCService;

public class CartaoCreditoController {
	
	public static void validaCartao( Context ctx ) {
		
		try {
			String body = ctx.body();
			Object validateResponse = CCService.validaCartao(body);
			
			ctx.json(validateResponse);
		} catch ( Exception error ) {
			
			ctx.json( error.getMessage() );
			error.printStackTrace();
		}
		
	}
	
}
