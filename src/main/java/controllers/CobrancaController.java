package controllers;


import java.util.ArrayList;
import io.javalin.http.Context;
import models.Cobranca;
import services.CobrancaService;

public class CobrancaController {

	private static ArrayList<Cobranca> filaCobranca = new ArrayList<Cobranca>();

//	Metodo post /cobranca
	public static void efetuaCobranca(Context ctx) {
		try {
			String body = ctx.body();
			Object cobrancaResponse = CobrancaService.efetuarCobranca(body);
			
			ctx.json(cobrancaResponse);
		} catch (Exception error) {
			
			ctx.json(error.getMessage());
			error.printStackTrace();
		}
	}

//	Metodo get /cobranca/{idCobranca} 
	public static void getCobranca(Context ctx) {		

		try {
			String pathParamId = ctx.pathParam("idCobranca");
			Object cobrancaResponse = CobrancaService.getCobranca(pathParamId);
			
			ctx.json(cobrancaResponse);
		} catch (Exception error) {
			
			ctx.json(error.getMessage());
			error.printStackTrace();
		}
	}

//	metodo POST /filaCobranca
	public static void addCobrancaLista(Context ctx) {

		try {
			String body = ctx.body();
			Object addResponse = CobrancaService.addCobrancaLista(body);

			ctx.json(addResponse);
		} catch (Exception error) {

			ctx.json(error.getMessage());
			error.printStackTrace();
		}
	}

}
