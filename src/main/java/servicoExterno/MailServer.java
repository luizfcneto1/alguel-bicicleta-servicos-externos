package servicoExterno;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class MailServer {
	
	private Session session = null;
	private Properties props;
	private MimeMessage mimeMessage;
	
	private static MailServer servicoEmail = new MailServer();
	
//	Singleton design pattern
	private MailServer() {
		this.props = System.getProperties();
		this.props.put("mail.smtp.port","587");
		this.props.put("mail.smtp.host", "smtp.gmail.com"); 
		this.props.put("mail.smtp.socketFactory.class",
			    "javax.net.ssl.SSLSocketFactory");
		this.props.put("mail.smtp.auth", "true");
		this.props.put("mail.smtp.starttls.enable", "true");
		
		session = Session.getDefaultInstance(props, null);
		session.setDebug(true);
	}
	
	public static MailServer getInstance() {
		return servicoEmail;
	}
	
	private void createMessage( String destinatario, String assunto, String mensagem ) {
		try {
			this.mimeMessage = new MimeMessage( this.session );
			this.mimeMessage.addRecipient( Message.RecipientType.TO, new InternetAddress( destinatario ));
			this.mimeMessage.setSubject( assunto );
						
			MimeMultipart multiPart = new MimeMultipart();
			MimeBodyPart bodyPart = new MimeBodyPart();
			bodyPart.setContent( mensagem, "text/html" );
			multiPart.addBodyPart( bodyPart );
			this.mimeMessage.setContent( multiPart );
			
		}catch( MessagingException error ) {
			error.printStackTrace();
		}
	}
	
	public void criaMensagem( String destinatario, String assunto, String mensagem ) {
		createMessage( destinatario, assunto, mensagem );
	}
	
	private void sendEmail() {
		String usuario = "devluizfcneto123@gmail.com";
		String senhaUsuario = "devluizfcneto";
		String emailHost = "smtp.gmail.com";
		
		try {
			Transport transport = session.getTransport("smtp");
			transport.connect( emailHost, usuario, senhaUsuario );
			transport.sendMessage( this.mimeMessage, this.mimeMessage.getAllRecipients() );
			transport.close();
			
		}catch( MessagingException error ) {
			error.printStackTrace();
		}
		
	}
	
	public void enviaEmail() {
		sendEmail();
	}
	
	
	
	
}
