package servicoExterno;

import controllers.CartaoCreditoController;
import controllers.CobrancaController;
import controllers.EmailController;
import io.javalin.*;

public class Main {

	public static void main(String[] args) {    		
		Javalin app = Javalin.create().start(getHerokuAssignedPort());
		
        app.post( "/enviarEmail", ctx -> {
        	EmailController.enviarEmail( ctx );
        });
        
        app.post( "/cobranca", ctx -> {
        	CobrancaController.efetuaCobranca( ctx );
        });  
        
        app.post( "/filaCobranca", ctx -> {
        	CobrancaController.addCobrancaLista(ctx);
        });

        app.get("/cobranca/:idCobranca", ctx -> {
        	CobrancaController.getCobranca(ctx);
        });
        
        app.post("/validaCartaoDeCredito", ctx -> {
        	CartaoCreditoController.validaCartao(ctx);
        });
        
        
	}
	
	private static int getHerokuAssignedPort() {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
          return Integer.parseInt(herokuPort);
        }
        return 7000;
      }
	
}
